#!/usr/bin/env bash

source build_functions.sh
#source <(curl -s https://example.com/script.sh)
#curl --remote-name $GITLAB_URL_PREFIX/$GITLAB_TEMPLATE_PREFIX/REPLACE_EXAMPLE_GITLAB_FOLDERNAME/-/raw/main/$SUB_FOLDERNAME/setup.sh

# Put here all the options your script accepts
CommandOptions=(
    '-c,--config <string:FILE>'
    '-l,--list'
    '-d,--debug <int:PORT>'
    '-r,--run <int:PORT>'
    '-v,--verbose'
    '-n,--dry-run'
    '-h,--help'
)

# Put here the corresponding descriptions for every option you specified in the array above
CommandDescriptions=(
    "Use the given configuration file instead of parameters"
    "List all program related files. if used with \`--verbose\`, the full contents are printed"
    "Run application and attach debugger"
    "Run application"
    "Turn on verbose output"
    "Don't actually do anything, only show what would be done"
    "Display help"
)

while getopts c:e: option
do
    case "$option" in
#        d) DebugOption=${OPTARG};;
#        r) RunOption=${OPTARG};;
        c) ConfigurationOption=${OPTARG};;
        e) EnvironmentNameOption=${OPTARG};;
        \?) show_usage;;
    esac
done

clear

echo "Print optional argument: $EnvironmentNameOption"

if [[ -z "$EnvironmentNameOption" ]]; then
    EnvironmentName="PRODUCTION"
    ASPNETCORE_ENVIRONMENT="PRODUCTION"
else
    EnvironmentName=${EnvironmentNameOption}
    ASPNETCORE_ENVIRONMENT=${EnvironmentNameOption}
fi

echo "EnvironmentName: ${EnvironmentName}"
echo "ASPNETCORE_ENVIRONMENT: ${ASPNETCORE_ENVIRONMENT}"

echo "Print optional argument: $Configuration"

if [[ -z "$ConfigurationOption" ]]; then
    ConfigurationName=${EnvironmentName}
else
    ConfigurationName=${ConfigurationOption}
fi

echo "Configuration: ${ConfigurationName}"

echo "Removing output directory..."
echo "----------------------------"
rm -r output

echo "Removing input directory..."
echo "----------------------------"
rm -r input;

echo "Removing bin directories..."
echo "----------------------------"
rm -r src/**/bin
rm -r src/hosts/**/bin

#echo "Clearing NuGet 3.x+ cache"
#echo "----------------------------"
#dotnet nuget locals http-cache --clear
#
#echo "Clearing NuGet global packages folder "
#echo "----------------------------"
#dotnet nuget locals global-packages --clear
#
#echo "Clearing NuGet temporary cache"
#echo "----------------------------"
#dotnet nuget locals temp --clear
#
#echo "Clearing NuGet plugins cache"
#echo "----------------------------"
#dotnet nuget locals plugins-cache --clear
#
#echo "Clearing all NuGet caches"
#echo "----------------------------"
#dotnet nuget locals all --clear

echo "Print optional argument: $Configuration"

if [[ $( get_runtime_configuration ) == "Release" ]]; then
  echo "Removing package lock files Release..."
  echo "----------------------------"

  dotnet restore --configfile nuget.config --use-lock-file --locked-mode
  get_success "Restoring Nuget packages with Lock File..."
else
  echo "Removing package lock files Debug..."
  echo "----------------------------"
#  rm -r src/**/packages.lock.json
#  rm -r src/hosts/**/packages.lock.json

  dotnet restore --configfile nuget.config --force-evaluate
  get_success "Restoring Nuget packages..."
fi

if [[ $( get_runtime_configuration ) != "Release" ]]; then
  echo "Formatting project..."
  echo "----------------------------"
  dotnet format --verify-no-changes --no-restore --report output/format/
fi

echo "Building project... for environment: $EnvironmentName ... with configuration: $( get_runtime_configuration ) ... on platform: $( get_platform ) ..."
echo "----------------------------"
dotnet build . --configuration $( get_runtime_configuration ) --verbosity minimal --property:TreatWarningsAsErrors=false --property:consoleloggerparameters=Summary --property:EnvironmentName=$EnvironmentName --no-restore --nologo
get_success "Building project..."

dotnet test . --logger "html;logfilename=testResults.html" --property:EnvironmentName=$EnvironmentName --no-build --results-directory output/test/
get_success "Running tests..."

echo "Publishing host projects..."
echo "----------------------------"

dotnet publish src/hosts/DotnetWeb.AzureFunction/DotnetWeb.AzureFunction.csproj --configuration $( get_runtime_configuration ) --property:EnvironmentName=$EnvironmentName --output output/publish/AzureFunction
get_success "Publishing Azure Function..."

dotnet publish src/hosts/DotnetWeb.ConsoleApplication/DotnetWeb.ConsoleApplication.csproj --configuration $( get_runtime_configuration ) --property:EnvironmentName=$EnvironmentName --output output/publish/ConsoleApplication
get_success "Publishing Console Application..."

dotnet publish src/hosts/DotnetWeb.WebApi/DotnetWeb.WebApi.csproj --configuration $( get_runtime_configuration ) --property:EnvironmentName=$EnvironmentName --output output/publish/WebApi
get_success "Publishing WebApi..."

dotnet pack src/DotnetWeb.Model/DotnetWeb.Model.csproj --configuration $( get_runtime_configuration ) --no-build --nologo --property:EnvironmentName=$EnvironmentName --output output/nuget/
get_success "Packing project..."

if [[ $( get_runtime_configuration ) == "Release" ]]; then
  mkdir -p input/nuget/
  echo $NUGET_SIGNING_CERTIFICATE >> input/nuget/signing-certificate.pfx

#  dotnet nuget sign output/nuget/*.nupkg --certificate-path input/nuget/signing-certificate.pfx -certificate-password $NUGET_SIGNING_PASSWORD --overwrite --output output/nuget/signed/
#  get_success "Creating signed NuGet packages..."

#  rm -r input;

  dotnet nuget verify output/nuget/*.nupkg  --configfile nuget.config
  get_success "Verifying the signed NuGet packages..."
fi

get_display_stats
get_success "Project!"

#!/bin/bash

usage() { echo "Usage: $0 [-s <string> (solution name)] [-p <string> (project prefix)] [-r <string> (folder name)] [-u <string> (git url)] [-t <string> (git template folder name)] [-l <string> (git dotfiles folder name)] [-g <string> (git group name)]" 1>&2; exit 1; }

while getopts s:p:r:u:t:l:g: option
do
    case "${option}"
    in
        s) SLN_NAME=${OPTARG};;
        p) FILE_NAME=${OPTARG};;
        r) GIT_ARTIFACT_FOLDER_NAME=${OPTARG};;
        u) GIT_URL_PREFIX=${OPTARG};;
        t) GIT_TEMPLATE_PREFIX=${OPTARG};;
        l) GIT_DOTFILES_LOCATION=${OPTARG};;
        g) GIT_GROUP_PREFIX=${OPTARG};;
        \?) usage;;
    esac
done

if [[ -z "$CI_PROJECT_DIR" ]]; then
    CI_PROJECT_DIR=$PWD
fi

WEBAPI=$FILE_NAME.WebApi
CONSOLE=$FILE_NAME.ConsoleApplication
AZUREFUNCTION=$FILE_NAME.AzureFunction
BOOTSTRAPPER=$FILE_NAME.Bootstrapper
ROUTE=$FILE_NAME.Route
CORE=$FILE_NAME.Core
COMMON=$FILE_NAME.Common
REPOSITORY=$FILE_NAME.Repository
MODEL=$FILE_NAME.Model

TEST_WEBAPI=$FILE_NAME.WebApi.Test
TEST_CONSOLE=$FILE_NAME.ConsoleApplication.Test
TEST_AZUREFUNCTION=$FILE_NAME.AzureFunction.Test
TEST_BOOTSTRAPPER=$FILE_NAME.Bootstrapper.Test
TEST_ROUTE=$FILE_NAME.Route.Test
TEST_CORE=$FILE_NAME.Core.Test
TEST_COMMON=$FILE_NAME.Common.Test
TEST_REPOSITORY=$FILE_NAME.Repository.Test
TEST_MODEL=$FILE_NAME.Model.Test

WEBAPI_PATH=src/hosts/$WEBAPI
CONSOLE_PATH=src/hosts/$CONSOLE
AZUREFUNCTION_PATH=src/hosts/$AZUREFUNCTION
BOOTSTRAPPER_PATH=src/$BOOTSTRAPPER
ROUTE_PATH=src/$ROUTE
CORE_PATH=src/$CORE
COMMON_PATH=src/$COMMON
REPOSITORY_PATH=src/$REPOSITORY
MODEL_PATH=src/$MODEL

TEST_WEBAPI_PATH=tests/$TEST_WEBAPI
TEST_CONSOLE_PATH=tests/$TEST_CONSOLE
TEST_AZUREFUNCTION_PATH=tests/$TEST_AZUREFUNCTION
TEST_BOOTSTRAPPER_PATH=tests/$TEST_BOOTSTRAPPER
TEST_ROUTE_PATH=tests/$TEST_ROUTE
TEST_CORE_PATH=tests/$TEST_CORE
TEST_COMMON_PATH=tests/$TEST_COMMON
TEST_REPOSITORY_PATH=tests/$TEST_REPOSITORY
TEST_MODEL_PATH=tests/$TEST_MODEL

WEBAPP_SDK_VERSION=9.0.101
WEBAPP_FRAMEWORK_VERSION=net9.0
LIBRARY_FRAMEWORK_VERSION=net9.0

if [ ! -d "$GIT_ARTIFACT_FOLDER_NAME" ]; then
  mkdir $GIT_ARTIFACT_FOLDER_NAME
fi

echo "CLONE FROM REPOSITORY"
git clone $GIT_URL_PREFIX/$GIT_TEMPLATE_PREFIX/dotnet-web.git

cd $GIT_ARTIFACT_FOLDER_NAME

curl --remote-name $GIT_URL_PREFIX/$GIT_TEMPLATE_PREFIX/$GIT_DOTFILES_LOCATION/-/raw/main/.gitattributes
curl --remote-name $GIT_URL_PREFIX/$GIT_TEMPLATE_PREFIX/$GIT_DOTFILES_LOCATION/-/raw/main/.gitignore
curl --remote-name $GIT_URL_PREFIX/$GIT_TEMPLATE_PREFIX/$GIT_DOTFILES_LOCATION/-/raw/main/.dockerignore
curl --remote-name $GIT_URL_PREFIX/$GIT_TEMPLATE_PREFIX/$GIT_DOTFILES_LOCATION/-/raw/main/gitlab-ci/dotnet/.editorconfig
curl --remote-name $GIT_URL_PREFIX/$GIT_TEMPLATE_PREFIX/$GIT_DOTFILES_LOCATION/-/raw/main/gitlab-ci/dotnet/.default-ci.yml
curl --remote-name $GIT_URL_PREFIX/$GIT_TEMPLATE_PREFIX/dotnet/-/raw/main/web/nuget.config
curl --remote-name $GIT_URL_PREFIX/$GIT_TEMPLATE_PREFIX/dotnet/-/raw/main/web/build.sh
curl --remote-name $GIT_URL_PREFIX/$GIT_TEMPLATE_PREFIX/dotnet/-/raw/main/web/Dockerfile
sed -i -e 's#REPLACE_EXAMPLE_PROJECTNAME#'"$SLN_NAME"'#g' .default-ci.yml
sed -i -e 's#REPLACE_EXAMPLE_PROJECTPREFIX#'"$FILE_NAME"'#g' .default-ci.yml
sed -i -e 's#REPLACE_EXAMPLE_PROJECTPREFIX#'"$FILE_NAME"'#g' Dockerfile
sed -i -e 's#REPLACE_EXAMPLE_SPECIFIC_GITLAB_PROJECTNAME#'"$GIT_GROUP_PREFIX/$GIT_TEMPLATE_PREFIX/$GIT_DOTFILES_LOCATION"'#g' .default-ci.yml
sed -i -e 's#REPLACE_EXAMPLE_PROJECTNAME#'"$SLN_NAME"'#g' build.sh

mv .default-ci.yml .gitlab-ci.yml

echo $CI_PROJECT_DIR/$GIT_ARTIFACT_FOLDER_NAME

dotnet new -i $CI_PROJECT_DIR/dotnet-web/src/hosts/DotnetWeb.WebApi
dotnet new -i $CI_PROJECT_DIR/dotnet-web/src/hosts/DotnetWeb.ConsoleApplication
dotnet new -i $CI_PROJECT_DIR/dotnet-web/src/hosts/DotnetWeb.AzureFunction
dotnet new -i $CI_PROJECT_DIR/dotnet-web/src/DotnetWeb.Bootstrapper
dotnet new -i $CI_PROJECT_DIR/dotnet-web/src/DotnetWeb.Route
dotnet new -i $CI_PROJECT_DIR/dotnet-web/src/DotnetWeb.Core
dotnet new -i $CI_PROJECT_DIR/dotnet-web/src/DotnetWeb.Common
dotnet new -i $CI_PROJECT_DIR/dotnet-web/src/DotnetWeb.Repository
dotnet new -i $CI_PROJECT_DIR/dotnet-web/src/DotnetWeb.Model

dotnet new -i $CI_PROJECT_DIR/dotnet-web/tests/DotnetWeb.WebApi.Test
dotnet new -i $CI_PROJECT_DIR/dotnet-web/tests/DotnetWeb.ConsoleApplication.Test
dotnet new -i $CI_PROJECT_DIR/dotnet-web/tests/DotnetWeb.AzureFunction.Test
dotnet new -i $CI_PROJECT_DIR/dotnet-web/tests/DotnetWeb.Bootstrapper.Test
dotnet new -i $CI_PROJECT_DIR/dotnet-web/tests/DotnetWeb.Route.Test
dotnet new -i $CI_PROJECT_DIR/dotnet-web/tests/DotnetWeb.Core.Test
dotnet new -i $CI_PROJECT_DIR/dotnet-web/tests/DotnetWeb.Common.Test
dotnet new -i $CI_PROJECT_DIR/dotnet-web/tests/DotnetWeb.Repository.Test
dotnet new -i $CI_PROJECT_DIR/dotnet-web/tests/DotnetWeb.Model.Test

echo "CREATE SOLUTION"
dotnet new sln -n $SLN_NAME

echo "CREATE GLOBALJSON"
dotnet new globaljson --sdk-version $WEBAPP_SDK_VERSION

echo "CREATE NUGET CONFIG"
dotnet new nugetconfig

echo "CREATE PROJECTS"
dotnet new emptyscope-dotnet-web-api -n $FILE_NAME -o $WEBAPI_PATH
dotnet new emptyscope-dotnet-web-console -n $FILE_NAME  -o $CONSOLE_PATH
dotnet new emptyscope-dotnet-web-function -n $FILE_NAME  -o $AZUREFUNCTION_PATH
dotnet new emptyscope-dotnet-web-bootstrapper -n $FILE_NAME -o $BOOTSTRAPPER_PATH
dotnet new emptyscope-dotnet-web-route -n $FILE_NAME -o $ROUTE_PATH
dotnet new emptyscope-dotnet-web-core -n $FILE_NAME -o $CORE_PATH
dotnet new emptyscope-dotnet-web-common -n $FILE_NAME -o $COMMON_PATH
dotnet new emptyscope-dotnet-web-repository -n $FILE_NAME -o $REPOSITORY_PATH
dotnet new emptyscope-dotnet-web-model -n $FILE_NAME -o $MODEL_PATH

dotnet new emptyscope-dotnet-web-api-test -n $FILE_NAME -o $$TEST_PATH
dotnet new emptyscope-dotnet-web-console-test -n $FILE_NAME  -o $$TEST_PATH
dotnet new emptyscope-dotnet-web-function-test -n $FILE_NAME  -o $$TEST_PATH
dotnet new emptyscope-dotnet-web-bootstrapper-test -n $FILE_NAME -o $$TEST_PATH
dotnet new emptyscope-dotnet-web-route-test -n $FILE_NAME -o $$TEST_PATH
dotnet new emptyscope-dotnet-web-core-test -n $FILE_NAME -o $$TEST_PATH
dotnet new emptyscope-dotnet-web-common-test -n $FILE_NAME -o $$TEST_PATH
dotnet new emptyscope-dotnet-web-repository-test -n $FILE_NAME -o $$TEST_PATH
dotnet new emptyscope-dotnet-web-model-test -n $FILE_NAME -o $$TEST_PATH

dotnet sln $SLN_NAME.sln add $WEBAPI_PATH/$WEBAPI.csproj $CONSOLE_PATH/$CONSOLE.csproj $AZUREFUNCTION_PATH/$AZUREFUNCTION.csproj $BOOTSTRAPPER_PATH/$BOOTSTRAPPER.csproj $ROUTE_PATH/$ROUTE.csproj $CORE_PATH/$CORE.csproj $COMMON_PATH/$COMMON.csproj $TEST_WEBAPI_PATH/$TEST_WEBAPI.csproj $TEST_CONSOLE_PATH/$TEST_CONSOLE.csproj $TEST_AZUREFUNCTION_PATH/$TEST_AZUREFUNCTION.csproj $TEST_BOOTSTRAPPER_PATH/$TEST_BOOTSTRAPPER.csproj $TEST_ROUTE_PATH/$TEST_ROUTE.csproj $TEST_CORE_PATH/$TEST_CORE.csproj $TEST_COMMON_PATH/$TEST_COMMON.csproj $TEST_REPOSITORY_PATH/$TEST_REPOSITORY.csproj $TEST_MODEL_PATH/$TEST_MODEL.csproj $REPOSITORY_PATH/$REPOSITORY.csproj $MODEL_PATH/$MODEL.csproj

echo "REMOVE TEMPLATE FOLDER"
rm -rf ../dotnet-web

echo "BUILD"
# TODO: switch to below build once https://github.com/dotnet/sdk/issues/2902 is merged
# dotnet build $SLN_NAME/$SLN_NAME.sln -c Release -f $WEBAPP_FRAMEWORK_VERSION
dotnet msbuild $SLN_NAME.sln -restore -target:Build -property:Configuration=Release -consoleloggerparameters:Summary -verbosity:minimal -nodeReuse:false -nologo

echo "TEST"
dotnet test $TEST_PATH/$TEST.csproj
dotnet test $TEST_WEBAPI_PATH/$TEST_WEBAPI.csproj
dotnet test $TEST_CONSOLE_PATH/$TEST_CONSOLE.csproj
dotnet test $TEST_AZUREFUNCTION_PATH/$TEST_AZUREFUNCTION.csproj
dotnet test $TEST_BOOTSTRAPPER_PATH/$TEST_BOOTSTRAPPER.csproj
dotnet test $TEST_ROUTE_PATH/$TEST_ROUTE.csproj
dotnet test $TEST_CORE_PATH/$TEST_CORE.csproj
dotnet test $TEST_COMMON_PATH/$TEST_COMMON.csproj
dotnet test $TEST_REPOSITORY_PATH/$TEST_REPOSITORY.csproj
dotnet test $TEST_MODEL_PATH/$TEST_MODEL.csproj

echo "PUBLISH"
dotnet publish $WEBAPI_PATH -c Release -f $WEBAPP_FRAMEWORK_VERSION -o $CI_PROJECT_DIR/publish

echo "REMOVE PUBLISH FOLDER"
cd ..
rm -rf publish

#!/usr/bin/env bash

function console_message () {
    local messageOption="$1"
    echo ${messageOption}

    return 0;
}

function find_pid() {
    local portOption="$1"
    lsof -i tcp:${portOption} | awk 'NR!=1 {print $2}'

    return 0;
}

function kill_port() {
    local portOption="$1"
    lsof -i tcp:${portOption} | awk 'NR!=1 {print $2}' | xargs kill

    return 0;
}

function rider_attach_process() {
    local portOption="$1"
    local slnDirectoryOption="$2"

    rider attach-to-process <pid> [<path-to-solution>]
    find_pid ${portOption} | xargs rider attach-to-process ${slnDirectoryOption}
    lsof -i tcp:${portOption} | awk 'NR!=1 {print $2}' | xargs kill

    return 0;
}

function file_exists(){
    local fileOption="$1"
    echo $fileOption
    [[ -f "fileOption" ]] && return 0 || return 1
}

function get_success () {
    if [[ "$?" -eq 0 ]]; then
        echo "----------------------------"
        echo "----------------------------"
        echo ""
        echo "Success: $?"
        echo ""
        return 0;
    else
        echo "----------------------------"
        echo "----------------------------"
        get_display_stats
        echo "----------------------------"
        echo "----------------------------"
        echo ""
        echo "Failure: $?"
        echo ""
        exit 1;
    fi
}

function get_platform () {
    case "${OSTYPE}" in
      solaris*) echo "SOLARIS" ;;
      darwin*)  echo "OSX" ;;
      linux*)   echo "LINUX" ;;
      bsd*)     echo "BSD" ;;
      msys*)    echo "WINDOWS" ;;
      cygwin*)  echo "ALSO WINDOWS" ;;
      *)        echo "UNKNOWN: $OSTYPE" return 1 ;;
    esac

    if [ get_success ]; then
        return 0;
    else
        return 1;
    fi
}

function get_display_stats () {
  echo ""
  echo "Environment: ${EnvironmentName}"
  echo "Configuration: ${ConfigurationName}"
  echo "ASPNETCORE_ENVIRONMENT: ${ASPNETCORE_ENVIRONMENT}"
  echo "Configuration: $( get_runtime_configuration )"
  echo "Dotnet: $( dotnet --version )"
  echo "OS: $( get_platform )"
  echo "Architecture: $( get_arch )"
  echo "Branch: $( git rev-parse --abbrev-ref HEAD )"
  echo "Hash: $( git rev-parse --verify HEAD )"
  echo "Date: $( date )"
  echo ""
}

function get_arch () {
    echo $(uname -m)

    if [ get_success ]; then
        return 0;
    else
        return 1;
    fi
}

function get_runtime_configuration () {
    if [[ -z "${EnvironmentName:u}" ]]; then
        echo 'Release'
        return 0;
    fi

    if [[ "${EnvironmentName:u}" == "LOCAL" ]]; then
        echo 'Debug'
    elif [[ "${EnvironmentName:u}" == "BUILD" ]]; then
        echo 'Debug'
    elif [[ "${EnvironmentName:u}" == "DEVELOPMENT" ]]; then
        echo 'Debug'
    elif [[ "${EnvironmentName:u}" == "QA" ]]; then
        echo 'Debug'
    else
        echo 'Release'
    fi

    return 0;
}

function usage () {
    # Put here the offset options will get
    local options_offset=5
    # Put here the offset descriptions will get after the longest option
    local descriptions_offset_after_longest_option=5
    # Put here the maximum length of descriptions spanning
    local maximum_descriptions_length=120

    # First we print the classic Usage message
    echo "Usage: $(basename "$0") [OPTION]..."
    # In the next loop, we put in ${max_option_length} the length of the
    # longest option. This way, we'll be able to calculate the offset when
    # printing long descriptions that should span over several lines.
    local max_option_length=1
    for (( i = 0; i < ${#CommandOptions[@]}; i++)); do
        if [[ $max_option_length -lt ${#CommandOptions[$i]} ]]; then
            max_option_length=${#CommandOptions[$i]}
        fi
    done
    # We put in the following variable the total offset of descriptions
    # after new-lines.
    local descriptions_new_line_offset=$((${max_option_length} + ${options_offset} + ${descriptions_offset_after_longest_option}))
    # The next loop is the main loop where we actually print the options with
    # the corresponding descriptions.
    for (( i = 0; i < ${#CommandOptions[@]}; i++)); do
        # First, we print the option and the offset we chose above right before it
        printf -- '%*s' ${options_offset}
        printf -- '%s' "${CommandOptions[$i]}"
        # Here we start tracking through out this loop the current index of the
        # char on the terminal window. This is necessary because in the process
        # of printing the descriptions' words we'll be able to know how not to
        # span over the defined maximum length or not to split words when
        # hitting ${COLUMNS}
        local current_char_index=$((${options_offset} + ${#CommandOptions[$i]}))
        # We calculate the offset which should be given between the current
        # option and the start of it's description. This is different for every
        # option because every option has a different length but they all must
        # be aligned according to the longest option's length and the offsets
        # we chose above
        local current_description_offset=$((${max_option_length} - ${#CommandOptions[$i]} + ${descriptions_offset_after_longest_option}))
        # We print this offset before printing the description
        printf -- '%*s' ${current_description_offset}
        # Updating the current_char_index
        current_char_index=$((${current_char_index} + ${current_description_offset}))
        # We put in a temporary variable the current description from the array
        local current_description="${CommandDescriptions[$i]}"
        # We divide the current_description to an array with the description's
        # words as the array's elements. This is necessary so we can print the
        # description without spliting words
        IFS=' ' read -r -a description_words <<< "${current_description}"
        # We start a loop for every word in the descriptions words array
        for (( j = 0; j < ${#description_words[@]}; j++)); do
            # We update the current char index before actually printing the
            # next word in the description because of the condition right
            # afterwards
            current_char_index=$((${current_char_index} + ${#description_words[$j]} + 1))
            # We check if the index we will reach will hit the maximum limit we
            # chose in the beginning or the number of ${COLUMNS} our terminal
            # gives us
            if [[ ${current_char_index} -le ${COLUMNS} ]] && [[ ${current_char_index} -le ${maximum_descriptions_length} ]]; then
                # If we don't hit our limit, print the current word
                printf -- '%s ' ${description_words[$j]}
            else
                # If we've hit our limit, print a new line
                printf -- '\n'
                # Print a number of spaces equals to the offset we need to give
                # according to longest option we have and the other offsets we
                # defined above
                printf -- '%*s' ${descriptions_new_line_offset}
                # print the next word in the new line
                printf -- '%s ' ${description_words[$j]}
                # Update the current char index
                current_char_index=$((${descriptions_new_line_offset} + ${#description_words[$j]}))
            fi
        done
        # print a new line between every option and it's description
        printf '\n'
    done

    return 0;
}
